[[_TOC_]]

# Lollipop Alfafa

Unicorns apparently Lollilop Alfafa for strong horns and magical energy. This container will be used initially for deploying [Zarf](https://zarf.dev/).

# Zarf Binaries

**[Zarf Releases](https://github.com/defenseunicorns/zarf/releases)**

Binaries needed for deployment: 
* Zarf Init
* Zarf Linux
