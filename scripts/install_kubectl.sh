#!/bin/bash

# Installing kubectl based on the guidance from https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/

# Download Binary
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"

# Download SHA
curl -LO "https://dl.k8s.io/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"

# Test SHA
echo "$(cat kubectl.sha256)  kubectl" | sha256sum --check

# Install Kubectl
install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

# Test and Validate install
kubectl version --client
