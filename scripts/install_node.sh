#!/bin/bash

NODEJS_VERSION=$1

# Download and extract the NodeJS binary
curl -OL https://nodejs.org/dist/v${NODEJS_VERSION}/node-v${NODEJS_VERSION}-linux-x64.tar.xz
tar -xf node-v${NODEJS_VERSION}-linux-x64.tar.xz
cd node-v${NODEJS_VERSION}-linux-x64

# Move relevant files to /usr/bin

mv bin/* /usr/bin/.
mv lib/* /usr/lib/.
mv include* /usr/include.
mkdir -p /usr/local/share/systemtap/tapset
mv share/systemtap/tapset/node.stp /usr/local/share/systemtap/tapset/.