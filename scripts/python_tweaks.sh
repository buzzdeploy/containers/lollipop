#!/bin/bash
set -euxo pipefail

apt-get update
DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt-get -y install tzdata
apt-get install -y software-properties-common curl
add-apt-repository --yes ppa:deadsnakes/ppa
apt-get update -qq
apt-get install -qqy python3.9
apt-get install -y python3.9-distutils build-essential python3.9-dev
curl https://bootstrap.pypa.io/get-pip.py | python3.9
# rm /usr/bin/python
# ls -lsa /usr/bin/py*
# ln -s /usr/bin/python3.9 /usr/bin/python3
